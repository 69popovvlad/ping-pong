﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using System;

public class GameClient : MonoBehaviour, INetEventListener
{
    public int myNetworkID = -1;

    // Connect options
    public string serverIP = "176.193.207.197";
    [HideInInspector] public int serverPort = 5000;
    public string serverKey = "kbBYMBNTHIIe", roomKey = "kbBYMBNTHIIe";

    [HideInInspector] public NetManager _netClient;
    private NetDataWriter _dataWriter;
    private NetPeer server;

    // Data recive
    private delegate void Packet_ (NetPacketReader reader);
    private static Dictionary<long, Packet_> packets;
    private readonly NetPacketProcessor _netPacketProcessor = new NetPacketProcessor();

    // public Stage curStage = Stage.Wait;

    private void InitializePackets()
    {
        packets = new Dictionary<long, Packet_>();
        packets.Add((long)PacketsName.GetIp, null);
        packets.Add((long)PacketsName.CreateRoom, CreateRoom);
    }

    void Awake()
    {
        InitializePackets();
    }

    void OnEnable()
    {
        _dataWriter = new NetDataWriter();
        _netClient = new NetManager(this);
        _netClient.Start();
        _netClient.UpdateTime = 15;
        _netClient.Connect(serverIP, serverPort, serverKey);
        
        //Subscribe to packet receiving
        _netPacketProcessor.SubscribeReusable<RacketPacket, NetPeer>(GetRacketPacket);
        _netPacketProcessor.SubscribeReusable<BallPacket, NetPeer>(GetBallPacket);
        _netPacketProcessor.SubscribeReusable<PlayerStatusPacket, NetPeer>(GetPlayerStatusPacket);

    }

    void Update()
    {
        _netClient.PollEvents();

        if(Input.GetKeyDown(KeyCode.Q))
        {
            _dataWriter.Reset();
            _dataWriter.Put(myNetworkID+"?"+roomKey);
            server.Disconnect(_dataWriter);
        }

        server = _netClient.FirstPeer;
        if (server != null && server.ConnectionState == ConnectionState.Connected)
        {
            
            // server.Send(_netPacketProcessor.Write(variables), DeliveryMethod.ReliableOrdered);
        }
        else
        {
            Debug.Log("Sending discovery packet to endpoint");
            _netClient.SendDiscoveryRequest(new byte[] { 1 }, serverPort);
        }
    }

    void OnDestroy()
    {
        if (_netClient != null)
            _netClient.Stop();
    }

    public void OnPeerConnected(NetPeer peer)
    {
        server = peer;
        Debug.Log("[CLIENT] We connected to " + peer.EndPoint);
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketErrorCode)
    {
        Debug.Log("[CLIENT] We received error " + socketErrorCode);
    }

    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        Packet_ packet;

        // Invoke function by identifier
        if(packets.TryGetValue(reader.GetLongWithOutChangePosition(), out packet))
        {
            reader.GetLong(); // Delete Long
            packet.Invoke(reader);
        }
        else
            _netPacketProcessor.ReadAllPackets(reader, peer);
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryResponse && _netClient.PeersCount == 0)
        {
            Debug.Log("[CLIENT] Received discovery response. Connecting to: " + remoteEndPoint);
            _netClient.Connect(serverIP, serverPort, serverKey);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {

    }

    public void OnConnectionRequest(ConnectionRequest request)
    {

    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log("[CLIENT] We disconnected because " + disconnectInfo.Reason);
    }











/// Rooms Functions //////////////////////////////////////////////

    // Client asked us to create a new Room
    public void CreateRoom(NetPacketReader packet)
    {
        // int networkID = packet.GetInt();
        // string nickName = packet.GetString();
        // string roomKey = packet.GetString();

        // // TODO: Проверку на наличие уже созданных комнат с таким RoomKey или с участием этого перца
        // allRooms.Add(new Room(roomKey, networkID, GetPeer(networkID)));
    }

/// End Rooms Functions //////////////////////////////////////////



/// Getting packets //////////////////////////////////////////////
    private void GetRacketPacket(RacketPacket packet, NetPeer peer)
    {
        peer.Send(_netPacketProcessor.Write(packet), DeliveryMethod.ReliableOrdered);
    }

    private void GetBallPacket(BallPacket packet, NetPeer peer)
    {

    }

    private void GetPlayerStatusPacket(PlayerStatusPacket packet, NetPeer peer)
    {

    }
/// End Getting packets //////////////////////////////////////////
}