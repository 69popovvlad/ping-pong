﻿public enum PlayerStatus {
    Searching,
    Ready,
    InBattle,
    Ending
}