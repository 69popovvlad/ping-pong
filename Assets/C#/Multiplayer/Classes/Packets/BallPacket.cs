﻿using UnityEngine;

public class BallPacket
{
    public int ID { get; set; }
    public Vector2 Position { get; set; }
}
