﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using LiteNetLib;
using LiteNetLib.Utils;

public class GameServer : MonoBehaviour, INetEventListener, INetLogger
{
    public string serverKey = "kbBYMBNTHIIe";
    public int serverPort = 5000;
    private NetManager _netServer;
    private List<NetPeer> allPeers = new List<NetPeer>();
    private NetDataWriter _dataWriter;
    private readonly NetPacketProcessor _netPacketProcessor = new NetPacketProcessor();

    // Data recive
    private delegate void Packet_ (NetPacketReader reader);
    private static Dictionary<long, Packet_> packets;

    private void InitializePackets()
    {
        packets = new Dictionary<long, Packet_>();
        packets.Add((long)PacketsName.GetIp, null);
        packets.Add((long)PacketsName.CreateRoom, CreateRoom);
    }

    void Awake() {
        InitializePackets();
    }

    void OnEnable()
    {
        NetDebug.Logger = this;
        _dataWriter = new NetDataWriter();
        _netServer = new NetManager(this);
        _netServer.Start(serverPort);
        _netServer.DiscoveryEnabled = true;
        _netServer.UpdateTime = 15;

        //Subscribe to packet receiving
        _netPacketProcessor.SubscribeReusable<RacketPacket, NetPeer>(GetRacketPacket);
        _netPacketProcessor.SubscribeReusable<BallPacket, NetPeer>(GetBallPacket);
        _netPacketProcessor.SubscribeReusable<PlayerStatusPacket, NetPeer>(GetPlayerStatusPacket);
    }

    void Update()
    {
        _netServer.PollEvents();
    }

    void OnDestroy()
    {
        NetDebug.Logger = null;
        if (_netServer != null)
            _netServer.Stop();
    }

    // К нам подключился второй игрок
    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log("[SERVER] We have new peer with NetworkID:" + peer.NetworkID + " " + peer.EndPoint);

        // Нужно спросить готов ли он (хотя если он подключился, тогда хз в чем трабл)
        // Отправка сообщения о начале боя (дать доступ к InputController)
        // Отправить спавн мяча
    }

    public void OnNetworkError(IPEndPoint endPoint, SocketError socketErrorCode)
    {
        Debug.Log("[SERVER] error " + socketErrorCode);
    }

    public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader,
        UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryRequest)
        {
            // Debug.Log("[SERVER] Received discovery request. Send discovery response");
            _netServer.SendDiscoveryResponse(new byte[] { 1 }, remoteEndPoint);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
    }

    public void OnConnectionRequest(ConnectionRequest request)
    {
        request.AcceptIfKey(serverKey);
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        Debug.Log("[SERVER] peer disconnected " + peer.EndPoint + ", info: " + disconnectInfo.Reason);
        
        // Прервать соединение окончательно и выйти в главное меню
    }

    // Receipting and distributing of packages
    public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
    {
        Packet_ packet;

        // Invoke function by identifier
        if(packets.TryGetValue(reader.GetLongWithOutChangePosition(), out packet))
        {
            reader.GetLong(); // Delete Long
            packet.Invoke(reader);
        }
        else
            _netPacketProcessor.ReadAllPackets(reader, peer);
    }

    public void WriteNet(NetLogLevel level, string str, params object[] args)
    {
        Debug.LogFormat(str, args);
    }










/// Rooms Functions //////////////////////////////////////////////

    // Client asked us to create a new Room
    public void CreateRoom(NetPacketReader packet)
    {
        // int networkID = packet.GetInt();
        // string nickName = packet.GetString();
        // string roomKey = packet.GetString();

        // // TODO: Проверку на наличие уже созданных комнат с таким RoomKey или с участием этого перца
        // allRooms.Add(new Room(roomKey, networkID, GetPeer(networkID)));
    }

/// End Rooms Functions //////////////////////////////////////////



/// Getting packets //////////////////////////////////////////////
    private void GetRacketPacket(RacketPacket packet, NetPeer peer)
    {
        peer.Send(_netPacketProcessor.Write(packet), DeliveryMethod.ReliableOrdered);
    }

    private void GetBallPacket(BallPacket packet, NetPeer peer)
    {

    }

    private void GetPlayerStatusPacket(PlayerStatusPacket packet, NetPeer peer)
    {

    }
/// End Getting packets //////////////////////////////////////////














/// My OLD functions /////////////////////////////////////////////
    public void DisconnectAll()
    {
        allPeers.Clear();
        _netServer.Stop();
    }
}
