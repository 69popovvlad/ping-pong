﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBall
{
    // Will be called when ball bounces of something
    void OnRebound();
}
