﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* The Ball memory */

[RequireComponent(typeof(SpriteRenderer))]
public class BallInfo : MonoBehaviour
{
    public IBall BallLogic;

    [Header("Characteristics")]
    [SerializeField] private float startSpeed = 1; // Start speed
    [SerializeField] private float maxSpeed = 12; // More than 12 can create bugs/glitches
    private float speed = 1; // Current speed

    void Awake()
    {
        speed = startSpeed;
        BallLogic = GetComponent(typeof(IBall)) as IBall;
    }

    #region Calling methods for getting and setting
    public float Speed {
        get{ return speed; }
        set{ speed = value > maxSpeed ? maxSpeed : value; }
    }
    #endregion
}
