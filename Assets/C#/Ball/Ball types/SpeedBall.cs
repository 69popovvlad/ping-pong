﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBall : MonoBehaviour, IBall
{
    [SerializeField] private float speedDelta = 0.1f;
    private BallInfo BallInfo;

    void Awake() { BallInfo = GetComponent<BallInfo>(); }

    public void OnRebound()
    {
        // Upping Ball's speed
        BallInfo.Speed += speedDelta;
    }
}
