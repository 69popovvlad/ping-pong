﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Ball movement logic */

[RequireComponent(typeof(Rigidbody2D))]
public class BallMovement : MonoBehaviour
{
    [SerializeField] private Vector2 curDirection;

    private BallInfo BallInfo;
    private Rigidbody2D rigidbody;
    private int reflectiveLayerIndex;

    void Awake()
    {
        reflectiveLayerIndex = LayerMask.NameToLayer("Reflective"); // For ray/collisions filtration
        rigidbody = GetComponent<Rigidbody2D>();
        BallInfo = GetComponent<BallInfo>();
    }

    void Start()
    {
        // Getting random vecto
        curDirection = GetRandomVector();

        rigidbody.velocity = curDirection * BallInfo.Speed;
    }

    // Getting random vector
    Vector2 GetRandomVector()
    {
        // Calculating random vector
        float xVector = Random.Range(-1f,1f);
        float yVector = Random.Range(-1f,1f);
        Vector2 calculatedVector = new Vector2(xVector, yVector);

        // Angle restrictions
        float angle = Vector2.Angle(calculatedVector, Vector2.up);
        if( (angle > 70 && angle < 110) || angle < 30 || angle > 150)
            calculatedVector = GetRandomVector();

        // Returning calculated vector
        calculatedVector.Normalize();
        return calculatedVector;
    }

    // When the Ball collides with a wall
    void OnCollisionEnter2D(Collision2D collision)
    {
        // Is it Reflactive object?
        if(collision.gameObject.layer == reflectiveLayerIndex)
        {
            // Yes, let's do movement logic
            curDirection = GetReflection() * BallInfo.Speed;
            if(curDirection == Vector2.zero)
                curDirection = GetRandomVector();
            rigidbody.velocity = curDirection;

            BallInfo.BallLogic.OnRebound(); // Calling interface method
        }
    }

    // Getting reflection or last direction
    Vector2 GetReflection()
    {
        // Sending the ray in the direction of flight (detecting only Wall)
        RaycastHit2D hit = Physics2D.Raycast(transform.position, curDirection, 256, 1 << reflectiveLayerIndex);
        if(hit.collider)
        {
            // Getting reflection
            Vector2 reflection = Vector3.Reflect(curDirection, hit.normal);

            #region Let's draw a little
                #if UNITY_EDITOR
                    // Debug.DrawRay(transform.position, curDirection, Color.red, 10);
                    Debug.DrawRay(hit.point, reflection, Color.yellow, 10);
                #endif
            #endregion
            
            // Return reflection
            reflection.Normalize();
            return reflection;
        }

        // Return last direction
        return curDirection;
    }
}
