﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class RacketMovement : MonoBehaviour
{
    [SerializeField] private float speed = 5;
    private Rigidbody2D rigidbody;
    
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    public void SetVelocity(float xVelocity)
    {
        rigidbody.velocity = new Vector2(xVelocity * speed, 0);
    }
}
