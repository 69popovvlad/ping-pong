﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Racket : MonoBehaviour
{
    [SerializeField] private MenuController menuController;
    private int ballLayerIndex;

    void Start()
    {
        ballLayerIndex = LayerMask.NameToLayer("Ball");
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == ballLayerIndex)
        {
            menuController.ScoreIncrease();
        }
    }
}
