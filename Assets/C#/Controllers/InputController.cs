﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RacketController))]
public class InputController : MonoBehaviour
{
    private RacketController racketController;
    private GameManager gameManager;
    private Camera cam;
    private Bounds bounds;

    void Awake()
    {
        racketController = GetComponent<RacketController>();
        cam = Camera.main;

        gameManager = GetComponent<GameManager>();
        bounds = gameManager.bounds;
    }

    void Update()
    {
        float x = 0;

        #if UNITY_EDITOR || UNITY_STANDALONE
            if(Input.GetMouseButton(0))
            {
                Vector2 touchPosition = cam.ScreenToWorldPoint(Input.mousePosition);
                float screenWidth = gameManager.bounds.extents.x - .2f;

                x = touchPosition.x/screenWidth;
                racketController.Distributor(x);
            }
            else
            {
                x = Input.GetAxis("Horizontal");
                racketController.Distributor(x);
            }
        #elif UNITY_IOS || UNITY_ANDROID
            if(Input.touchCount > 0)
            {
                Vector2 touchPosition = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
                // float screenWidth = gameManager.bounds.extents.x - .2f;

                x = touchPosition.x > 0 ? 1 : -1;
                racketController.Distributor(x);
            }
            else
                racketController.Distributor(0); // Stopping if not inputs
        #endif
    }
}
