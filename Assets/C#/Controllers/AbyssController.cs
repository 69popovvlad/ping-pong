﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbyssController : MonoBehaviour
{
    private GameManager gameManager;
    private MenuController menuController;
    private int ballLayerIndex;
    public bool isItMyAbyss;

    void Awake()
    {
        ballLayerIndex = LayerMask.NameToLayer("Ball");
        gameManager = FindObjectOfType<GameManager>();
        menuController = FindObjectOfType<MenuController>();

    }

    // Will be called when Ball collisions with this abyss (gate)
    void OnTriggerEnter2D(Collider2D collider)
    {
        // Is it Ball?
        if(collider.IsTouchingLayers(ballLayerIndex))
        {
            if(menuController.isItOnline)
            {
                // Yes, let's give score
                menuController.ScoreIncrease(!isItMyAbyss); // isItMyAbyss == !isItMyScore
                gameManager.EndGame();
            }
            else
            {
                gameManager.EndGame();
            }

            Destroy(collider.gameObject, 2);
        }
    }
}
