﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [Header("Interfaces")]
    [SerializeField] private GameObject inGameInterface;
    [SerializeField] private GameObject mainMenuInterface;

    [Header("Interfaces 'settings'")]
    [SerializeField] private GameObject ballPanelPrefab;
    [SerializeField] private Transform ballsContent; // Content into Scroll View
    [SerializeField] private GameObject offlineSelected, onlineSelected;

    [Header("Texts")]
    [SerializeField] private Text lastScore;
    [SerializeField] private Text bestScore;
    [SerializeField] private Text gameScore;

    [HideInInspector] public bool isItOnline;

    private int myScore, enemyScore; // Scores keepers
    private bool isItJustPause;
    private List<GameObject> ballPanel_selectedObjects = new List<GameObject>(); // Cach

    void Start()
    {
        SpawnBallPanels();
        ResetParameters();
    }

    public void ResetParameters()
    {
        myScore = 0;
        enemyScore = 0;
        UpdateGameScore(0, 0);
        UpdateLastScore(PlayerPrefs.GetInt("LastScore"));
        UpdateBestScore(PlayerPrefs.GetInt("BestScore"));
        ChoosingTypeOfGame(PlayerPrefs.GetInt("GameType") == 1 ? true : false); // Not cool :c
        ChoosingBall(PlayerPrefs.GetInt("BallType"));
    }

    // Spawning ball panels in interface
    void SpawnBallPanels()
    {
        for(int i = 0; i < gameManager.ballPrefabs.Count; i++)
        {
            GameObject ball = gameManager.ballPrefabs[i];
            SpriteRenderer ballImage = ball.GetComponent<SpriteRenderer>();

            // Spawning ball panel in Content of Scroll View (Balls)
            GameObject ballPanel = Instantiate(ballPanelPrefab, ballsContent);
            ballPanel.name = ball.name;

            // Replacing image and delegating button.onClick
            ballPanel.GetComponent<Image>().sprite = ballImage.sprite;
            Button ballPanel_buttron = ballPanel.GetComponent<Button>();
            ballPanel_buttron.interactable = true;
            int ballIndex = i;
            ballPanel_buttron.onClick.AddListener(() => ChoosingBall(ballIndex));

            // Some work with visualization of selecting
            GameObject selected = ballPanel.transform.GetChild(0).gameObject;
            ballPanel_selectedObjects.Add(selected);
            selected.SetActive(false);
        }

        // Spawning ball is not available (this is my whim)
        GameObject notAvailableBall = Instantiate(ballPanelPrefab, ballsContent);
        notAvailableBall.transform.GetChild(0).gameObject.SetActive(false);
    }

    // Will be called when Pause button is pressed
    public void Pause()
    {
        Time.timeScale = 0;
        SwitchInterfaces(true);
        isItJustPause = true;
    }

    // Will be called when Resume button is pressed
    public void Resume()
    {
        Time.timeScale = 1;
        SwitchInterfaces(false);

        // Is it NOT game pause?
        if(!isItJustPause)
        {
            ResetParameters();
            gameManager.gameObject.SetActive(true);
        }
    }

    // Switching between Main and Game interfaces 
    public void SwitchInterfaces(bool isMainMenuActive)
    {
        mainMenuInterface.SetActive(isMainMenuActive);
        inGameInterface.SetActive(!isMainMenuActive);
    }

    // Will be called when Ball is selected
    public void ChoosingBall(int id)
    {
        PlayerPrefs.SetInt("BallType", id);

        // Turning off all without one 
        ballPanel_selectedObjects.ForEach(selected => selected.SetActive(false));
        if(id != -1) // Not random ball
            ballPanel_selectedObjects[id].SetActive(true);
    }

    // Will be called when Game type is selected
    public void ChoosingTypeOfGame(bool isItOnline)
    {
        // Is it game pause?
        if(isItJustPause) return;

        PlayerPrefs.SetInt("GameType", isItOnline ? 1 : 0);

        // Switching between Offline and Online games 
        offlineSelected.SetActive(!isItOnline);
        onlineSelected.SetActive(isItOnline);

        this.isItOnline = isItOnline;
        gameManager.GetComponent<RacketController>().isItOnline = isItOnline;
    }

    #region Score's text and PlayerPrefs updating
    public void UpdateGameScore(string text) { gameScore.text = text; }
    
    public void UpdateGameScore(int myScores, int enemyScores)
    {
        gameScore.text = myScores+":"+enemyScores;
    }

    public void UpdateLastScore(int scores)
    {
        PlayerPrefs.SetInt("LastScore", scores);
        lastScore.text = "Last " + scores; 

        int bestScore = PlayerPrefs.GetInt("BestScore");
        if(scores > bestScore)
            UpdateBestScore(scores);
    }

    public void UpdateBestScore(int scores)
    {
        PlayerPrefs.SetInt("BestScore", scores);
        bestScore.text = "Best " + scores;
    }
    #endregion

    public void ScoreIncrease(bool isItMyScore = true)
    {
        if(isItOnline)
        {
            if(isItMyScore)
                myScore++;
            else
                enemyScore++;

            UpdateGameScore(myScore, enemyScore);
        }
        else
        {
            myScore++;
            UpdateLastScore(myScore); // To PlayerPrefs
            UpdateGameScore(myScore.ToString()); // Visualization
        }
    }
}
