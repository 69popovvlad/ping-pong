﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacketController : MonoBehaviour
{
    [Header("Controll settings")]
    [SerializeField] private List<RacketMovement> racketObjects = new List<RacketMovement>();
    public bool isItOnline;

    public void Distributor(float xVelocity, bool isItLocal = true)
    {
        if(isItOnline) // Online
        {
            OnlineController(xVelocity, isItLocal);
        }
        else // Offline
        {
            OfflineController(xVelocity);
        }
    }

    void OnlineController(float xVelocity, bool isItLocal = true)
    {
        if(isItLocal)
        {
            SendVelocity(xVelocity);
        }
        else
        {
            // Accept the actions of another player
        }
    }

    void SendVelocity(float xVelocity)
    {
        // Send to other player and visualize on our device
    }

    void OfflineController(float xVelocity)
    {
        foreach(RacketMovement racketMovement in racketObjects)
            racketMovement.SetVelocity(xVelocity);
    }
}
