﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private MenuController menuController;

    [Header("Spawn settings")]
    public List<GameObject> ballPrefabs = new List<GameObject>();
    [SerializeField] private GameObject wallPrefab, abyssPrefab;
    [SerializeField] private Transform wallsParent, ballsParent; // Store objects of walls and balls

    [HideInInspector] public int score;

    [HideInInspector] public Bounds bounds;
    private Camera cam;
    private InputController inputController;

    void Start()
    {
        // Disabling detecting a ball colliding with itself (ray)
        int ballLayerIndex = LayerMask.NameToLayer("Ball");
        Physics2D.IgnoreLayerCollision(ballLayerIndex, ballLayerIndex);

        // Getting Camera bouns
        cam = Camera.main;
        bounds = Camera2DBounds();

        // Borders spawning
        SpawnWalls();  
        SpawnAbysses();
    }
    
    void OnEnable()
    {
        // Let's go
        StartCoroutine(Timer());
    }

    public void SpawnBallAt(Vector2 position)
    {
        // Getting ball index
        int ballIndex = Random.Range(0, ballPrefabs.Count);
        if(!menuController.isItOnline)
        {
            int ballIndex_playerPrefs = PlayerPrefs.GetInt("BallType");
            ballIndex = ballIndex_playerPrefs == -1 ? ballIndex : ballIndex_playerPrefs;
        }

        // Spawning ball at position
        GameObject ball = Instantiate(ballPrefabs[ballIndex], position, Quaternion.identity);
        ball.transform.parent = ballsParent;
    }

    // Before the game
    IEnumerator Timer()
    {
        // Some monkey-code
        menuController.UpdateGameScore("2");
        yield return new WaitForSeconds(1);
        menuController.UpdateGameScore("1");
        yield return new WaitForSeconds(1);
        menuController.UpdateGameScore("GO");

        // Spawning ball
        SpawnBallAt(bounds.center);
    }

    // Wall spawning
    void SpawnWalls()
    {
        Vector3 wallScale = wallPrefab.transform.localScale;
        wallPrefab.transform.localScale = new Vector3(wallScale.x, bounds.extents.y * 16, 1);

        // Spawning right Wall
        GameObject rightWall = Instantiate(wallPrefab, new Vector2(bounds.extents.x, 0), Quaternion.identity);
        rightWall.transform.parent = wallsParent;
        rightWall.name = "Right wall";

        // Spawning left Wall
        GameObject leftWall = Instantiate(wallPrefab, new Vector2(-bounds.extents.x, 0), Quaternion.identity);
        leftWall.transform.parent = wallsParent;
        rightWall.name = "Left wall";
    }

    // Abyss spawning
    void SpawnAbysses()
    {
        Vector3 abyssScale = abyssPrefab.transform.localScale;
        abyssPrefab.transform.localScale = new Vector3(bounds.extents.x * 16, abyssScale.x, 1);

        // Spawning upper abyss
        GameObject upperAbyss = Instantiate(abyssPrefab, new Vector2(0, bounds.extents.y), Quaternion.identity);
        upperAbyss.transform.parent = wallsParent;
        upperAbyss.name = "Upper abyss";
        upperAbyss.GetComponent<AbyssController>().isItMyAbyss = true; // Only for my abyss (gate)

        // Spawning lower abyss
        GameObject lowerAbyss = Instantiate(abyssPrefab, new Vector2(0, -bounds.extents.y), Quaternion.identity);
        lowerAbyss.transform.parent = wallsParent;
        lowerAbyss.name = "Lower abyss";
    }

    Bounds Camera2DBounds()
	{
		float height = cam.orthographicSize * 2;
		return new Bounds(Vector3.zero, new Vector3(height * cam.aspect, height, 0));
	}

    public void EndGame()
    {
        menuController.ResetParameters();
        menuController.SwitchInterfaces(true);
        gameObject.SetActive(false);
    }
}
