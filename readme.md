# Ping-Pong
### Game controls
- Mobile devices
  - By holding to the left and right of the center of the screen. The farther from the center of the screen, the faster your racket
- Other devices
  - Arrows or A,D buttons
  - Also by clicking on the left and right of the center of the screen. The farther from the center of the screen, the faster your racket
----
### About Game
<img src="./images/screen.png" alt="screenshot" width=250>

Balls:<br />
- Grey -  random ball
- Yellow - standart ball
- Speed - faster with each touch, up to a certain limit
- purple - in a collision, it splits (not available now)

Game types:<br />
- Offline - you control two rackets at the same time
- Online - you compete with another player up to 7 points (not available now)
----
### About me
Vladislav P.<br />
[My site]

<!-- Identifiers, in alphabetical order -->

[My site]: https://69popovvlad.gitlab.io/cv "Click-click-click"